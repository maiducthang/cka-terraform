resource "google_project_service" "bucket" {
  project = var.project_id
  service = "compute.googleapis.com"
}

resource "google_service_account" "bucket" {
  account_id   = "openvpn-service-account"
  display_name = "OpenVPN Service Account"
  project      = var.project_id
}

resource "google_project_iam_binding" "openvpn_storage_binding" {
  project = var.project_id
  role    = "roles/storage.objectAdmin"

  members = [
    "serviceAccount:${google_service_account.bucket.email}",
  ]

  depends_on = [google_service_account.bucket]
}