#!/bin/bash

sudo apt-get update && mkdir -p /home/ubuntu &&
cd /home/ubuntu &&
curl -O https://raw.githubusercontent.com/kwisser/create-cloud-compute-engine-ovpn-server/main/openvpn-install.sh &&
chmod +x openvpn-install.sh && sudo AUTO_INSTALL=y ./openvpn-install.sh &&
mv /root/client.ovpn /home/ubuntu &&
gsutil cp /home/ubuntu/client.ovpn gs://cka-dev-devops-bucket/client.ovpn