provider "google" {
  project     = var.project_id
  region      = var.region
  credentials = var.credentials
}

terraform {
  required_providers {
    ansible = {
      version = "~> 1.1.0"
      source  = "ansible/ansible"
    }
  }
}