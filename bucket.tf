module "bucket" {
  source        = "./modules/gcp_bucket"
  name          = "devops"
  project_id    = var.project_id
  environment   = var.environment
  region        = var.region
  project_name  = var.project_name
  force_destroy = true
  storage_class = "STANDARD"
  location      = "ASIA-SOUTHEAST1"
}