resource "google_compute_address" "bastion_ip" {
  name = "${var.project_name}-bastion-ip"
}