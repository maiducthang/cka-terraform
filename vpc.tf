module "vpc" {
  source             = "./modules/gcp_networking"
  environment        = var.environment
  project            = var.project_id
  region             = var.region
  project_name       = var.project_name
  public_cidr_range  = var.public_cidr_range
  private_cidr_range = var.private_cidr_range
}