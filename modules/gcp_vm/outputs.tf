output "id" {
  value = google_compute_instance.this.*.id
}

output "instance_id" {
  value = google_compute_instance.this.*.instance_id
}

output "name" {
  value = google_compute_instance.this.*.name
}

output "self_link" {
  value = google_compute_instance.this.*.self_link
}

output "network_ip" {
  value = google_compute_instance.this.*.network_interface.0.network_ip
}

output "ssh_user" {
  value = var.username
}