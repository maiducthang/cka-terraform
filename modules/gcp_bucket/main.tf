locals {
  name = "${var.project_name}-${var.environment}"
  labels = {
    "project" : var.project_name
    "environment" : var.environment
    "author" : "terraform"
  }
}


resource "google_storage_bucket" "this" {
  name          = "${local.name}-${var.name}-bucket"
  location      = var.location
  project       = var.project_id
  force_destroy = var.force_destroy
  storage_class = var.storage_class
  labels        = local.labels

  versioning {
    enabled = var.versioning
  }
}