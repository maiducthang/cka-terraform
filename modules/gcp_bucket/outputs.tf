output "name" {
  value = google_storage_bucket.this.name
}

output "self_link" {
  value = google_storage_bucket.this.self_link
}

output "url" {
  value = google_storage_bucket.this.url
}