variable "project_name" {
  type        = string
  description = "The GCP project name"
}

variable "project_id" {
  type        = string
  description = "The GCP project id"
}

variable "environment" {
  type        = string
  description = "The project environment"
}

variable "region" {
  type        = string
  description = "The GCP region that infrastructure deploy in"
}

variable "name" {
  type        = string
  description = "The bucket name"
}

variable "force_destroy" {
  type        = bool
  description = "Whether to allow the bucket and all objects in it to be deleted"
  default     = false
}

variable "storage_class" {
  type        = string
  description = "The storage class of the bucket"
  default     = "STANDARD"
}

variable "location" {
  type        = string
  description = "The location of the bucket"
  default     = "US"
}

variable "versioning" {
  type        = bool
  description = "Enable versioning for the bucket"
  default     = false
}

