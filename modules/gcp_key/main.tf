locals {
  name = "${var.project_name}-${var.environment}-${var.name}"
}

resource "tls_private_key" "ssh" {
  algorithm = "RSA"
  rsa_bits  = "4096"
}

resource "local_file" "private_key" {
  filename        = "${var.path}/${local.name}.pem"
  file_permission = "600"
  content         = tls_private_key.ssh.private_key_pem
}

resource "local_file" "public_key" {
  filename        = "${var.path}/${local.name}.pub"
  file_permission = "600"
  content         = tls_private_key.ssh.public_key_openssh
}