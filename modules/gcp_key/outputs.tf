output "private_key" {
  value = local_file.private_key.filename
}

output "public_key" {
  value = local_file.public_key.filename
}
