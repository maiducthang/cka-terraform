variable "project_name" {
  type        = string
  description = "The project name"
}

variable "environment" {
  type        = string
  description = "The project environment"
}

variable "name" {
  type        = string
  description = "The SSH key name"
}

variable "path" {
  type        = string
  description = "The folder to save ssh key"
  default     = ""
}