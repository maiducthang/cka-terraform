resource "local_file" "ansible_inventory" {
  content = templatefile("${path.root}/templates/hosts.tftpl", {
    master-id      = module.master.id
    master-ip      = module.master.network_ip
    master-dns     = module.master.name
    master-user    = module.master.ssh_user
    master-ssh-key = element(split("/", module.master_key.private_key), 3)

    worker-id      = module.worker.id
    worker-ip      = module.worker.network_ip
    worker-dns     = module.worker.name
    worker-user    = module.worker.ssh_user
    worker-ssh-key = element(split("/", module.worker_key.private_key), 3)
  })
  filename = "${path.root}/hosts"
}

resource "local_file" "bastion_startup_script" {
  filename = "${path.root}/files/k8s_bastion.sh"
  content = templatefile("${path.root}/templates/bastion.tftpl", {
    username    = var.username
    bucket_name = module.bucket.name
  })
}

resource "time_sleep" "wait_for_bastion_init" {
  depends_on      = [module.bastion_host]
  create_duration = "30s"
  triggers = {
    "always_run" = timestamp()
  }
}

resource "null_resource" "provisioner" {
  depends_on = [
    local_file.ansible_inventory,
    time_sleep.wait_for_bastion_init,
    module.bastion_host
  ]

  triggers = {
    "always_run" = timestamp()
  }

  provisioner "file" {
    source      = "${path.root}/hosts"
    destination = "/home/${var.username}/hosts"
    connection {
      type        = "ssh"
      host        = google_compute_address.bastion_ip.address
      user        = var.username
      private_key = file("~/.ssh/id_rsa")
      agent       = false
      insecure    = true
    }
  }
}

resource "null_resource" "copy_ansible_playbooks" {
  depends_on = [
    null_resource.provisioner,
    time_sleep.wait_for_bastion_init,
    module.bastion_host
  ]

  triggers = {
    "always_run" = timestamp()
  }

  provisioner "file" {
    source      = "${path.root}/files/ansible"
    destination = "/home/${var.username}/"

    connection {
      type        = "ssh"
      host        = google_compute_address.bastion_ip.address
      user        = var.username
      private_key = file("~/.ssh/id_rsa")
      insecure    = true
      agent       = false
    }
  }
}

resource "null_resource" "copy_private_ssh_key" {
  depends_on = [
    null_resource.provisioner,
    module.master,
    module.worker,
    time_sleep.wait_for_bastion_init
  ]
  provisioner "file" {
    source      = "${path.root}/files/ssh-key/"
    destination = "/home/${var.username}/.ssh/"

    connection {
      type        = "ssh"
      host        = google_compute_address.bastion_ip.address
      user        = var.username
      private_key = file("~/.ssh/id_rsa")
      insecure    = true
      agent       = false
    }
  }
}

resource "null_resource" "run_ansible" {
  depends_on = [
    null_resource.provisioner,
    null_resource.copy_private_ssh_key,
    module.master,
    module.worker,
    time_sleep.wait_for_bastion_init
  ]

  triggers = {
    always_run = timestamp()
  }

  connection {
    type        = "ssh"
    host        = google_compute_address.bastion_ip.address
    user        = var.username
    private_key = file("~/.ssh/id_rsa")
    agent       = false
    insecure    = true
  }
  provisioner "remote-exec" {
    inline = [
      "echo install ansible",
      "sudo apt update -y",
      "sudo apt install software-properties-common -y",
      "sudo add-apt-repository --yes --update ppa:ansible/ansible",
      "sudo apt install ansible -y",
      "chmod 600 ~/.ssh/*.pem",
      "echo 'starting ansible playbooks...'",
      "sleep 10 && ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i /home/${var.username}/hosts /home/${var.username}/ansible/playbook.yml ",
    ]
  }
}