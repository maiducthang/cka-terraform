module "worker_key" {
  source       = "./modules/gcp_key"
  name         = "worker"
  path         = "./files/ssh-key"
  environment  = var.environment
  project_name = var.project_name
}

module "master_key" {
  source       = "./modules/gcp_key"
  name         = "master"
  path         = "./files/ssh-key"
  environment  = var.environment
  project_name = var.project_name
}

